import React, { Component } from 'react';
import Register from './Components/Register';
import Login from './Components/Login';

class App extends Component {
  render() {
    return (
      <div>
        <Login/>
        <Register/>
      </div>
    )
  }
}

export default App;
