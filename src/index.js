import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Route, BrowserRouter as Router, Switch, Redirect } from 'react-router-dom'
import Login from './Components/Login'
import Register from './Components/Register'
import Notfound from './Components/Notfound'
import SplitButton from './Components/Try'
import PersistentDrawerLeft from './Components/Drawer'
import {Dashboard} from './Components/Dashboard'
// import Authenticated from './Components/Authenticated'
import SMaster from './Components/SMaster'
import Client from './Components/Client'
// import {Dashboard} from './Components/Dashboard'
// import {ProtectedRoute} from './Components/ProtectedRoute'
// var authenticated = window.localStorage.getItem('isAuthenticated');
// var usercheck= JSON.parse(authenticated)
export const routing = (
  <Router>
    <div>
    <Switch>
      {/* <Authenticated /> */}
      <Redirect exact from="/" to="login" />
      <Route path="/login" component={Login} />
      <Route path="/register" component={Register} />
      <Route path="/try" component={SplitButton} />
      <Route exact path="/" component={SMaster}/>
      <Route path="/drawer" component={PersistentDrawerLeft} />
      {/* <ProtectedRoute path="/dashboard" component={Dashboard} /> */}
      <Route path="/dashboard" component={Dashboard} />
      <Route path="/add" component={Client} />
      <Route component={Notfound} />
      </Switch>
    </div>
  </Router>
)
ReactDOM.render(
  // <App />,
  routing,
  document.getElementById('root')
);
