import React, { Component } from 'react'
import { Route, BrowserRouter as Router } from 'react-router-dom'
import Login from './Login'
import {Dashboard} from './Dashboard'
import CryptoJS from 'crypto-js'

class Authenticated extends Component {
    constructor(props) {
        var checkauthentication=CryptoJS.AES.decrypt(window.localStorage.getItem('isAuthenticated'), 'secret key 123');
        var authenticate=JSON.parse(checkauthentication.toString(CryptoJS.enc.Utf8));
        super(props)
    
        this.state = {
            isAuthenticated:authenticate,
            path:'',
            component:''
        }
    }
    dashboard() {
        <Router>
            <Route path="/dashboard" component={Dashboard} />
        </Router>
    }
    loginpage() {
        <Router>
            <Route path="/login" component={Login} />
        </Router>
    }
    render() {
        const {isAuthenticated} = this.state
        return (
            <div>
                {isAuthenticated ? 
                    this.dashboard()
                :
                    this.loginpage()
                }
            </div>
        )
    }
}

export default Authenticated
