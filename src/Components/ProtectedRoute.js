import React from 'react'
import {Route, Redirect} from 'react-router-dom'
import auth from './auth'
import CryptoJS from 'crypto-js'
var checkauthentication=CryptoJS.AES.decrypt(window.localStorage.getItem('isAuthenticated'), 'secret key 123');
var authenticate=JSON.parse(checkauthentication.toString(CryptoJS.enc.Utf8));
export const ProtectedRoute = ({component: Component, ...rest}) => {
    return(
        
        <Route {...rest} render={
            (props) => {
                // if(auth.isAuthenticated()) {
                    if(authenticate){
                    return <Component {...props}/>
                }
                else{
                    return <Redirect to={
                        {
                            pathname:'/login',
                            state:{
                                from:props.location
                            }
                        }
                    }
                    />
                }
            }
        }/>
    )
}