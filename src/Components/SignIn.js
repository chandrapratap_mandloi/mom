import React, { Component } from 'react'
// import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
// import Typography from '@material-ui/core/Typography';
// import { Person } from '@material-ui/icons';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import profile from '../images/profile.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faEnvelope} from '@fortawesome/free-solid-svg-icons';
import {faLock} from '@fortawesome/free-solid-svg-icons';
import '../CSS/login.css'
class SignIn extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             email:''
        }
    }
    
    render() {
        return (
            <div class="container-fluid">
                <div>
                    <h1>NeoSoft</h1>
                </div>
                <div>
                    <small>
                        Sign in to continue
                    </small>
                </div>
                <div className="paperdiv">
                <Paper className="Paper">
                <form  noValidate autoComplete="off">
                    <div>
                        <img src={profile} alt="Logo" />
                    </div>
                    
                        <div>
                            <TextField
                            error={false}
                                id="outlined-name"
                                label={<FontAwesomeIcon icon={faEnvelope} className="image"/>}
                                margin="normal"
                                variant="outlined"
                                style={{ margin: 8 }}
                                placeholder="Your Email"
                                InputLabelProps={{
                                    shrink: true,
                                  }}
                            />
                        </div>
                        <div>
                            <TextField
                                id="outlined-password-input"
                                label={<FontAwesomeIcon icon={faLock} className="image"/>}
                                type="password"
                                autoComplete="current-password"
                                margin="normal"
                                variant="outlined"
                                style={{ margin: 8 }}
                                placeholder="Password" 
                                InputLabelProps={{
                                    shrink: true,
                                  }}
                            />
                        </div>
                        <div>
                            <Button variant="contained" color="primary" >
                                Primary
                            </Button>
                        </div>
                    </form>
                </Paper>
                </div>
                <div>
                    <a href="#">Create an account</a>
                    {/* <small>Create account</small> */}
                </div>
            </div>
        )
    }
}

export default SignIn
