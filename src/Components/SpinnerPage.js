import React from "react";
import { css } from '@emotion/core';
import HashLoader from 'react-spinners/HashLoader';
// import { textAlign } from "@material-ui/system";
// Can be a string as well. Need to ensure each key-value pair ends with ;

const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;
 
class SpinnerPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    //   loading: true
    }
  }
  render() {
    return (
        //  id ="center" style="position:fixed;top:50%;left:50%"
      <div style={{position:'fixed', top:'40%', left:'50%', zIndex:99}} className='sweet-loading'>
        <HashLoader
          css={override}
          sizeUnit={"px"}
          size={150}
          color={'#3f51b5'}
          // loading={true}
          loading={this.props.localData.loading}
        />
      </div> 
    )
  }
}

export default SpinnerPage;