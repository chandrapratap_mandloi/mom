import React, { Component } from 'react';
import BaseURL from '../BaseURL';
import axios from 'axios';
import Paper from '@material-ui/core/Paper';
import EditIcon from '@material-ui/icons/Edit'
import EditComment from './EditComment'
import SpinnerPage from './SpinnerPage'
import Decrypt from './Decrypt'
import Button from '@material-ui/core/Button';
// import { toast } from 'react-toastify';
// import 'react-toastify/dist/ReactToastify.css';
import Grid from '@material-ui/core/Grid';
import SendReport from './SendReport'
class SMaster extends Component {
    constructor(props) {
        super(props)
        // var user = window.localStorage.getItem('user');
        this.state = {
            userData: Decrypt().user,
            data: [],
            modal: false,
            reportmodal:false,
            loading: true,
            selectedIndex:undefined
        }
    }
    fetchData() {
        const { userData } = this.state
        const AuthStr = (userData.data.token);
        axios.get(`${BaseURL}/tasks/todaytask`, { headers: { Authorization: AuthStr } })
        .then(response => {
            if(response.data.success===true){
               this.setState({
                    loading: false,
                   data: response.data.data
               })
            }
            else{
                this.setState({
                    loading: false
                })
            }
        })
        .catch((error) => {
            this.setState({ loading: false})
            console.log('error ', error);
        });
    }
    modalclose = (value, isUpdated) => {
        this.setState({
            modal: value
        })
        if (isUpdated) {
            this.fetchData()
        }
    }

    componentDidMount(){
        this.fetchData()
    }
    addComment (index) {
        this.setState({modal: true,selectedIndex:index})
    }
    reportmodalclose = (value, isUpdated) => {
        this.setState({
            reportmodal: value
        })
        if (isUpdated) {
            this.fetchData()
        }
    }
    // sendReport(data) {
    //     console.log(data)
    //     this.setState({reportmodal: true})
    // }
    sendReport(data) {
        console.log(data)
        this.setState({reportmodal: true})
    }
    // sendReport (task,index) {
    //     const {userData} = this.state
    //     const data = {
    //         currentDate: Date.now(),
    //         name: task.userName,
    //         task: task.task,
    //         help:task.help,
    //         issues:task.issues
    //     }
    //     console.log(task)
    //         // alert("Form submitted")
    //         const headers = {
    //             'Authorization': userData.data.token
                
    //         }
    //         // ${BaseURL}/tasks/sendreport
    //         axios.post(`//localhost:5000/api/tasks/sendreport`, data, {
    //             headers: headers
    //         })
    //             .then(response => {
    //                 console.log('****',response.data)
    //                 toast.success(" Report Sent", { autoClose: 7000 })
    //             })
    //             .catch(error => {
    //                 console.log(error.data, 'error')
    //                 this.setState({ loading: false })
    //             })
    // }
    showUsersTask() {
        const {data} = this.state
        console.log(data)
        // const classes = useStyles();
        return (
            <div>
                <Grid container spacing={3}>
                {/* <Button onClick={()=>this.sendReport(data)} style={{color:"white"}} >
                            Send Report
                </Button> */}

                <Grid item xs={12} style={{textAlign:'right'}}>
                { <SendReport cbClose={this.reportmodalclose} reportdata={data}  isVisible={this.state.reportmodal} />}
                <Button  onClick={()=>this.sendReport(data)} style={{ marginBottom: '2%',color: 'white', backgroundColor:'#1d89e4' }} variant="contained" >
                    Send Report
                </Button></Grid>
            <table >
            <tbody>
                <tr style={{ backgroundColor: '#1d89e4', color:'white' }}>
                    <th>Name</th>
                    <th>Task</th>
                    <th>Client</th>
                    <th>Issue</th>
                    <th>Priority</th>
                    <th>Comment</th>
                    {/* <th>Send Report</th> */}
                </tr>
                {data.map((tasks,index)=> {
                    const bg = tasks.help === 'High' ? {backgroundColor: 'blue',flex:2} : tasks.help === 'Medium' ? { backgroundColor: 'yellow',flex:2} : { backgroundColor: 'green',flex:2}
                    return(
                        <tr key={index} style={{ backgroundColor: 'coral' , color:'white'}}>
                    <td>
                        {tasks.userName}  
                    </td>
                    <td>
                        {tasks.task}
                    </td>
                    <td>
                      {tasks.client}
                    </td>
                    <td >
                        {tasks.issues}
                    </td>
                    <td key={index}>
                        <div style={{display: "flex",flex: 1,flexdirection: "row"}}>
                            <div style={bg}></div>
                            <div style={{flex: 20}}>{tasks.help}</div>
                        </div>
                    </td>
                    <td>
                        {tasks.adminComment}
                        {this.state.selectedIndex===index && <EditComment userId={tasks._id} adminComment={tasks.comment}  cbClose={this.modalclose}  isVisible={this.state.modal} />}
                        {/* onClick={()=>this.addComment(tasks,index)} */}
                        {/* onClick={() => this.setState({ modal: true })} */}
                        <EditIcon onClick={()=>this.addComment(index)} style={{height: 11 ,paddingLeft : 10}}/>
                    </td>
                    {/* <td>
                        <Button onClick={()=>this.sendReport(tasks,index)} style={{color:"white"}} >
                            Send Report
                        </Button>
                    </td> */}
                </tr>
                    )
                }
                )}
            </tbody>
        </table>
        </Grid>
        </div>
        )
    }
    handleUSerTasks(){
        return(
            <div>
                No User has added task
            </div>
        )
    }
    render() {
        const {data} = this.state
        return (
            <div style={{display:'flex', justifyContent:'center', marginTop:'10%'}}>
                <div>
                    <SpinnerPage localData={this.state} />
                </div>
                <Paper >
                {/* <Button  >style={{width:'80%'}}
                    User's Task
                </Button> */}
                    { data.length === 0 ? this.handleUSerTasks() : this.showUsersTask() }
                </Paper>
            </div>
        )
    }
}

export default SMaster
