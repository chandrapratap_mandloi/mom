import React, { Component } from 'react'
import axios from 'axios'
import BaseURL from '../BaseURL';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import '../CSS/client.css'
import Button from '@material-ui/core/Button';
import AddcircleOutlineIcon from '@material-ui/icons/AddCircle'
import TextField from '@material-ui/core/TextField';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SpinnerPage from './SpinnerPage';
import CryptoJS from 'crypto-js'
import Decrypt from './Decrypt'
import Checkbox from '@material-ui/core/Checkbox';
class Client extends Component {
    constructor(props) {
        super(props)
        // var task_id = window.localStorage.getItem('task');
        // var user = window.localStorage.getItem('user');
        var taskid=CryptoJS.AES.decrypt(window.localStorage.getItem('task'), 'secret key 123');
        var task_id=JSON.parse(taskid.toString(CryptoJS.enc.Utf8));
        this.state = {
            data: [],
            taskid: task_id,
            userData: Decrypt().user,
            toggle: false,
            client: '',
            startDate: '',
            endDate: '',
            errors: {},
            loading: false,
            checked:false,
            selectedIndex:undefined
        }
    }
    // UNSAFE_componentWillMount
    fetchData() {
        const { userData } = this.state
        const AuthStr = (userData.data.token);
        // users/client/:id
        // tasks/client
        axios.get(`${BaseURL}/users/client/${userData.data.user._id}`, { headers: { Authorization: AuthStr } })
            .then(response => {
                this.setState({
                    data: response.data.data,
                    loading: false
                })
            })
            .catch((error) => {
                console.log('error ', error);
                this.setState({ loading: false })
            });
    }
    componentDidMount() {
        this.fetchData()
        this.setState({ loading: true })
        
    }
    handleChange = (event) => {
        this.setState({
            client: event.target.value
        })
    }
    
    // handleCheckboxChange =() =>{
    //     this.setState({
    //         checked : ! this.state.checked
    //     },console.log(this.state.checked,'???'))
    // }
    handleCheckboxChange (index,data) {
        const { userData, taskid } = this.state
        const clientdata = {
            client: data,
            startDate: this.state.startDate,
            endDate: this.state.endDate
        }
        const headers = {
            'Authorization': userData.data.token
        }
        this.setState({
            checked : ! this.state.checked,
            selectedIndex:index
        }, () =>
        axios.put(`${BaseURL}/tasks/client/${taskid}`, clientdata, {
            headers: headers
        })
            .then(response => {
                if (response.data.success === true) {
                    this.setState({
                        toggle: false,
                        loading: false
                    },this.fetchData())
                }
                // console.log(response)
            })
            .catch(error => {
                console.log(error.data, 'error')
                this.setState({ loading: false })
            })
        )
    }
    handleStartdate = (event) => {
        this.setState({
            startDate: event.target.value
        })
    }
    handleEnddate = (event) => {
        this.setState({
            endDate: event.target.value
        })
    }
    // onClick={()=>this.setState({toggle:false})}
    handleValidation = () => {
        let errors = {};
        let formIsValid = true;
        var nametext = document.getElementById('outlined-name').value;
        if (nametext.length === 0) {
            formIsValid = false;
            toast.error('Add Client!!', { autoClose: 7000 })
            errors["task"] = "Add Client!!!";
        }
        else {
            formIsValid = true;
            errors["task"] = "";
        }
        this.setState({ errors: errors });
        return formIsValid;
    }
    
    handleSubmit = (event) => {
        this.setState({ loading: true })
        const { userData, taskid } = this.state
        event.preventDefault()
        const data = {
            client: this.state.client,
            startDate: this.state.startDate,
            endDate: this.state.endDate,
        }
        if (this.handleValidation()) {
            // alert("Form submitted")
            const headers = {
                'Authorization': userData.data.token
            }
            axios.put(`${BaseURL}/tasks/client/${taskid}`, data, {
                headers: headers
            })
                .then(response => {
                    if (response.data.success === true) {
                        this.setState({
                            toggle: false,
                            loading: false
                        },this.fetchData())
                        
                    }
                })
                .catch(error => {
                    console.log(error.data, 'error')
                    this.setState({ loading: false })
                })
        }
        else {
            // alert("Form has errors.")
            this.setState({ loading: false })
        }

    }
    addClient = () => {
        return (
            <div >
                {/* <Paper style={{ display: 'flex', justifyContent: 'space-around' }}> */}
                    <form
                        // ref="form"
                        onSubmit={this.handleSubmit}
                        className="formstyle"
                        onError={errors => console.log('error', errors)}
                    >
                        <Typography variant="h5" component="h3">
                            <TextField
                                className="texvalid"
                                id="outlined-name"
                                type="text"
                                label={<AddcircleOutlineIcon />}
                                name="name"
                                margin="normal"
                                variant="outlined"
                                onChange={this.handleChange}
                                value={this.state.client}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                placeholder="Add Client"
                                autoComplete='text'
                            />
                        </Typography>
                        {/* <Typography variant="h5" component="h3">
                            <TextField
                                id="date"
                                label="Start Date"
                                variant="outlined"
                                type="date"
                                margin="normal"
                                onChange={this.handleStartdate}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            /></Typography>
                        <Typography variant="h5" component="h3">
                            <TextField
                                id="date"
                                label="End Date"
                                type="date"
                                margin="normal"
                                variant="outlined"
                                onChange={this.handleEnddate}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            /></Typography> */}
                        <Typography style={{paddingLeft: '2%',alignSelf:'center'}} variant="h5" component="h3">
                            <Button type="submit" style={{ color: 'white', backgroundColor:'#1d89e4' }} variant="contained" color="primary">
                                Submit
                        </Button>
                        </Typography>
                    </form>
                {/* </Paper> */}
            </div>
            //   <h2>Hello</h2>
        )
    }
    showClients = () => {
        const { data,checked } = this.state
        return (
            <div className="subdiv">
                <Button onClick={() => this.setState({ toggle: true })} style={{ marginBottom: '2%',color: 'white', backgroundColor:'#1d89e4' }} variant="contained" >
                    Add Client
                    </Button>
                {data.map((data, index) =>
                    <Paper key={index} style={{backgroundColor:'#1d89e4', color:'white'}}>
                        <Checkbox
                            checked={this.state.selectedIndex===index && checked}
                            onClick={()=>this.handleCheckboxChange(index,data)}
                            value="checked"
                            // color="primary"
                            inputProps={{
                                'aria-label': 'secondary checkbox',
                            }}
                        />
                        <Typography key={index} variant="h5" component="h3">
                            {data}
                        </Typography>
                    </Paper>
                )}
            </div>

        )
    }
    render() {
        const { toggle } = this.state
        return (
            <div className="maindiv">
                <div>
                    <ToastContainer autoClose={7000} />
                    <SpinnerPage localData={this.state} />
                </div>
                {/* backgroundColor: '#ff7f50', */}
                <Paper style={{  width: '80%', justifyContent: 'center', textAlign: 'right' }} >
                    {toggle ? this.addClient() :  this.showClients()}
                </Paper>
            </div>
        )
    }
}

export default Client
