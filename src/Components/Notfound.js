// import React from 'react'

// const Notfound = () => <h1>Page Not found</h1>
import React, { Component } from 'react'
import Paper from '@material-ui/core/Paper';
// import { flexbox } from '@material-ui/system';
import Grid from '@material-ui/core/Grid';
export class Notfound extends Component {
    render() {
        return (
            <div style={{display:'flex', justifyContent:'center', paddingTop:'5%'}}>
                <Paper>
                <Grid container>
                
                    <img src={require('../images/notfound.jpg')} style={{height:400, width:550}} alt="sign up" />
                    <Grid item xs={12}>
                        <a href="/dashboard">Go to the Homepage</a>
                    </Grid>
                </Grid>
                </Paper>
            </div>
        )
    }
}

export default Notfound

// export default Notfound
