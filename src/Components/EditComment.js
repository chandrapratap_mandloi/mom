import React, { Component } from 'react'
import Modal from 'react-modal';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import 'react-toastify/dist/ReactToastify.css';
// import BaseURL from '../BaseURL';
import axios from 'axios';
import Decrypt from './Decrypt'
// import CryptoJS from 'crypto-js'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';

const customStyles = {
  content: {
    // backgroundColor: '#ff7f50',
    backgroundColor: '#1d89e4',
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
};
class EditComment extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      modalIsOpen: this.props.isVisible,
      errors: {},
      // taskid: task_id,
      adminComment:this.props.adminComment,
      userData: Decrypt().user,
    }
    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }
  openModal() {
    this.setState({ modalIsOpen: true });
  }

  afterOpenModal() {
    this.subtitle.style.color = '#f00';
  }

  closeModal() {
    this.props.cbClose(false)
    // this.setState({ modalIsOpen: false, toggle: false });
  }
  
  handleClientChange = (event) => {
    this.setState({
      client: event.target.value
    })
  }
  handleissueChange = (event) => {
    this.setState({
      issues: event.target.value
    })
  }
  handlePriorityChange = (event) => {
    this.setState({
      help: event.target.value
    })
  }
  componentDidMount() {
    Modal.setAppElement('body');
  }


  handleSubmit = (event) => {
    const { userData } = this.state
    event.preventDefault()
    const data = {
        adminComment:this.state.adminComment
    }
      const headers = {
        'Authorization': userData.data.token
      }
    //   5d67cf58602709000479017d
    // ${userData.data.user._id}
    // localhost:5000/api/tasks/adminComment/5d6e6ed592584400043e2918
    axios.put(`//localhost:5000/api/tasks/adminComment/${this.props.userId}`, data, {
      // axios.put(`${BaseURL}/tasks/adminComment/${this.props.userId}`, data, {
        headers: headers
      })
        .then(response => {
          if (response.data.success) {
            this.props.cbClose(false, response.data.success)
            // this.closeModal()
          }
        })
        .catch(error => {
          console.log(error.data, 'error')
        })
  }
    handleChange = (event) => {
        this.setState({
            adminComment: event.target.value
        })
    }
  render() {
    // const { data } = this.state
    return (
      <Modal
        isOpen={this.props.isVisible}
        // onAfterOpen={this.afterOpenModal}
        // onRequestClose={this.closeModal}
        style={customStyles}
        contentLabel="Example Modal"
      >
        <CloseIcon onClick={this.closeModal} />
        <Paper >
        <ValidatorForm
            ref="form"
            onSubmit={this.handleSubmit}
            onError={errors => console.log('error', errors)}
        >
            <Grid container style={{ display: 'flex', justifyContent: 'center' }}  spacing={4} >
              <Grid item xs={12} style={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="h5" component="h3">
                  Add Comment
                  </Typography>
              </Grid>
              <Grid item xs={12} style={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="h5" component="h3">
                  <TextValidator
                            label="Add Comment"
                            type="text"
                            name="name"
                            margin="normal"
                            variant="outlined"
                            onChange={this.handleChange}
                            id="email"
                            autoComplete="email"
                            value={this.state.adminComment}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            validators={['required']}
                            errorMessages={['this field is required']}

                        />
                </Typography>
              </Grid>
              <Grid item style={{ textAlign: 'center', alignSelf: 'center' }}>
                <Typography>
                  <Button type="submit" variant="contained" style={{backgroundColor:'#1d89e4',color:'white'}} >
                    Submit
            </Button>
                </Typography>
              </Grid>
            </Grid>
            </ValidatorForm>
          {/* </form> */}
        </Paper>
      </Modal>

    )
  }
}

export default EditComment
