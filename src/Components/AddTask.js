import React, { Component } from 'react'
// import { ValidatorForm} from 'react-material-ui-form-validator';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser } from '@fortawesome/free-solid-svg-icons'
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import axios from 'axios'
import BaseURL from '../BaseURL';
import Grid from '@material-ui/core/Grid';
import {  toast } from 'react-toastify';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
// import SpinnerPage from './SpinnerPage';ToastContainer,
import 'react-toastify/dist/ReactToastify.css';
import Decrypt from './Decrypt'
import '../CSS/dashboard.css'

const style={
    display:'flex',
    justifyContent:'center'
}
class AddTask extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userData: Decrypt().user,
            task: '',
            errors: {}
        }
    }
    handleTaskChange = (event) => {
        this.setState({
            task: event.target.value
        })
    }
    handleValidation = () => {
        let errors = {};
        let formIsValid = true;
        var nametext = document.getElementById('outlined-name').value;
        if (nametext.length === 0) {
            formIsValid = false;
            toast.error('Add Task!!', { autoClose: 7000 })
            errors["task"] = "Task Cannot be empty";
        }
        else {
            formIsValid = true;
            errors["task"] = "";
        }
        this.setState({ errors: errors });
        return formIsValid;
    }
    handleSubmit = (event) => {
        const { userData } = this.state
        event.preventDefault()
        const data = {
            task: this.state.task,
            currentDate: Date.now()
        }
        if (this.handleValidation()) {
            // alert("Form submitted")
            const headers = {
                'Authorization': userData.data.token
            }
            axios.post(`${BaseURL}/tasks`, data, {
                headers: headers
            })
                .then(
                    toast.success('Task Added', { autoClose: 7000 }),
                    // window.localStorage.setItem('flag',true),
                    window.localStorage.setItem('flag', false),
                    this.props.cb(false)
                )
                .catch(error => {
                    console.log(error.data, 'error')
                })
        }
    }
    render() {
        return (
            <div className="carddiv">
                
                <Paper style={{ backgroundColor: '#ff7f50' }}>
                    <form
                        onSubmit={this.handleSubmit}
                        className="addtaskform"
                    >
                        <Paper>
                        <Grid container spacing={3}>
                            {/* <Grid container> */}
                                <Grid item xs={12} style={style}>
                                    <Typography variant="h5" component="h3">
                                        Add Task
                    </Typography>
                                </Grid>
                                <Grid item xs={12} style={style}>
                                    <Typography variant="h5" style={{ height: 43 }} component="h3">
                                    <TextareaAutosize 
                                        // aria-label="empty textarea" 
                                        // placeholder="Empty" 
                                        aria-label="minimum height" 
                                        rows={3}
                                        id="outlined-name"
                                        type="text"
                                        label={<FontAwesomeIcon icon={faUser} className="image" />}
                                        name="name"
                                        variant="outlined"
                                        onChange={this.handleTaskChange}
                                        value={this.state.task}
                                        placeholder="Add Task"
                                        autoComplete='text'
                                    />
                                        {/* <TextField
                                            style={{ height: 37, width: '95%' }}
                                            // className="texvalid"
                                            id="outlined-name"
                                            type="text"
                                            label={<FontAwesomeIcon icon={faUser} className="image" />}
                                            name="name"
                                            // margin="normal"
                                            variant="outlined"
                                            onChange={this.handleTaskChange}
                                            value={this.state.task}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            placeholder="Add Task"
                                            autoComplete='text'
                                        /> */}
                                    </Typography></Grid>
                                    <Grid item xs={12} style={style}>
                                    <Typography component="p">
                                        <Button type="submit" style={{ width: 110,  backgroundColor:'#1d89e4' }} variant="contained" color="primary">
                                            Add Task
                                        </Button>
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Paper>
                    </form>
                </Paper>
            </div>
        )
    }
}

export default AddTask
