import React, { Component } from 'react'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import '../CSS/dashboard.css'
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
// import Grid from '@material-ui/core/Grid';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import HomeIcon from '@material-ui/icons/Home'
import AddcircleOutlineIcon from '@material-ui/icons/AddCircle'
import HelpOutlineIcon from '@material-ui/icons/Help'
import EditIcon from '@material-ui/icons/Edit'
import HighlightOff from '@material-ui/icons/HighlightOff'
// import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ErrorOutlineIcon from '@material-ui/icons/Error'
import { toast } from 'react-toastify';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import 'react-toastify/dist/ReactToastify.css';
import BaseURL from '../BaseURL';
import axios from 'axios'
import AddTask from './AddTask';
// import VerticalTabs from './Tab'
import SpinnerPage from './SpinnerPage'
import EditTask from './EditTask'
import Client from './Client'
import Issue from './Issue'
import Priority from './Priority'
import SMaster from './SMaster'
import CryptoJS from 'crypto-js'
import Decrypt from './Decrypt'
// import auth from './auth'
export class Dashboard extends Component {

    constructor(props) {
        super(props)
        // var bytes=CryptoJS.AES.decrypt(window.localStorage.getItem('user'), 'secret key 123');
        // var plaintext=JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
        // var user = window.localStorage.getItem('user');
        var showdash = window.localStorage.getItem('flag');
        this.state = {
            
            userData: Decrypt().user,
            // dashboardData:dash,
            setOpen: false,
            flag: false,
            local:JSON.parse(showdash),
            data: [],
            modal: false,
            loading: true,
            drawercontent: '',
            edittoggle: false
        }
    }
    fetchData() {
        const { userData } = this.state
        const AuthStr = (userData.data.token);
        axios.get(`${BaseURL}/tasks/${userData.data.user._id}`, { headers: { Authorization: AuthStr } })
            .then(response => {
                if (response.data.length === 0) {
                    this.setState({
                        loading: false,
                        data: response.data.data
                    })
                }
                else {
                    this.setState({
                        data: response.data.data,
                        loading: false
                    })
                }
                
            })
            .catch((error) => {
                console.log('error ', error);
                window.localStorage.setItem('flag', true)
                this.setState({
                    flag: true,
                    loading:false
                })
            });
    }
    componentDidMount() {
        var isAuthenticated = window.localStorage.getItem('isAuthenticated')
        if(isAuthenticated){    
            this.fetchData()
        }
        else{
            this.props.history.push('/login')     
        }  
    }
    handleDrawerOpen = () => {
        this.setState({ setOpen: true });
    }
    handleDrawerClose = () => {
        this.setState({ setOpen: false })
    }
    logout = () => {
        localStorage.removeItem('user');
        localStorage.removeItem('flag');
        localStorage.removeItem('task');
        localStorage.removeItem('isAuthenticated');
        this.props.history.push('/login')
        toast.success("Logout Successfully", { autoClose: 7000 })
        // setTimeout(() => { toast.success("Logout Successfully", { autoClose: 3000 }) }, 10)
        // auth.logout(() =>{
        //     this.props.history.push('/login')
        // })
    }
    
    modalclose = (value, isUpdated) => {
        this.setState({
            modal: value
        })
        if (isUpdated) {
            this.fetchData()
        }
    }
    showUserTask = () => {
        const { data } = this.state
        const dash = data[data.length - 1]
        // window.localStorage.setItem('user',CryptoJS.AES.encrypt(JSON.stringify(response.data),'secret key 123'))
        data.length > 0 && window.localStorage.setItem('task', CryptoJS.AES.encrypt(JSON.stringify(dash._id),'secret key 123'))
        return (
            <div className="tablediv">
                {/* <div>
                    <ToastContainer autoClose={7000} /> */}
                    {/* <SpinnerPage localData={this.state}/> */}
                {/* </div> */}
                {data.length > 0 && <EditTask cbClose={this.modalclose} help={dash.help} client={dash.client} issues={dash.issues} task={dash.task} isVisible={this.state.modal} />}
                {/* <Paper className="paperdiv"> */}
                    {data.length > 0 && <table>
                        <tbody>
                            <tr style={{ backgroundColor: '#1d89e4', color:'white' }}>
                                <th>Task</th>
                                <th>Client</th>
                                <th>Issue</th>
                                <th>Help</th>
                                <th>Actions</th>
                            </tr>
                            <tr style={{ backgroundColor: 'coral' , color:'white'}}>
                                <td>
                                    {
                                        dash.task}
                                </td>

                                <td>
                                    {dash.client}
                                </td>
                                <td>
                                    {dash.issues}
                                </td>
                                <td >
                                    {dash.help}
                                </td>
                                <td >
                                    {/* edittoggle */}
                                    <EditIcon onClick={() => this.setState({ modal: true })} />
                                    {/* <button onClick={() => this.setState({ modal: true })} className="button1">Edit</button> */}
                                </td>
                            </tr>
                        </tbody>
                    </table>}
                {/* </Paper> */}
            </div>
        )
    }
    
    successcb = (val) => {
        if(!val)
        {
            this.setState({local:val},this.fetchData())
            document.getElementById('disabled').disabled = val;
            // this.fetchData()
        }
        
    }
    project(str) {
        const { local } = this.state
        // document.getElementById('disabled').disabled = true
        switch (str) {
            // cb={this.successcb()}
            case "Home": return <div>{local ? <AddTask disable={true}  cb={this.successcb} /> : this.showUserTask()} </div>;
            case "Current Client": return <Client />;
            case "User Issues": return <Issue />;
            case "Help": return <Priority />;
            // case "SMaster": return <SMaster/>;
            default: return (
                <div>
                    {local  ? <AddTask cb={this.successcb}/> : this.showUserTask()}
                </div>
            )
        }
    }
    render() {
        const { local,drawercontent, userData } = this.state
        return (

            <div>
                {/* <Grid container  style={{backgroundColor:'Gray', height:'100%'}}> */}
                <div>
                    <SpinnerPage localData={this.state} />
                </div>
                <div className="root">
                    <AppBar style={{backgroundColor:'#1d89e4'}} position="static">
                        <Toolbar>
                            <div className="MuiToolbar-root">
                           { userData && userData.data.user.role==='scrummaster' ?
                                <IconButton
                                    className="menuButton"
                                    edge="start"
                                    color="inherit"
                                    aria-label="menu"
                                    
                                >
                                     <SupervisorAccountIcon />
                                    {/* <MenuIcon /> */}
                                </IconButton> :
                                <IconButton
                                    className="menuButton"
                                    edge="start"
                                    color="inherit"
                                    aria-label="menu"
                                    onClick={this.handleDrawerOpen}
                                >
                                     <MenuIcon />
                                </IconButton>}
                                <Typography className="title" variant="h6" >
                                    {userData && userData.data.user.name}
                                </Typography>
                            </div>
                            <div>
                                <Button color="inherit" onClick={this.logout}>Logout <HighlightOff /></Button>
                            </div>
                        </Toolbar>
                    </AppBar>
                </div>
                {userData && userData.data.user.role==='scrummaster' ? <SMaster/> :this.project(drawercontent)}
                {/* <div>
                    {flag ? <AddTask/> : this.showUserTask()}
                </div> */}
                <div>
                    <Drawer
                        variant="persistent"
                        anchor="left"
                        open={this.state.setOpen}
                    >
                        <div>
                            <IconButton onClick={this.handleDrawerClose}>
                                <ChevronLeftIcon />
                            </IconButton>
                        </div>
                        <Divider />
                        <List style={{color:'#1d89e4'}}>
                            <ListItem key={1} onClick={() => this.setState({ drawercontent: 'Home' })} button >
                                <ListItemIcon style={{color:'#1d89e4'}} ><HomeIcon /></ListItemIcon>
                                <ListItemText primary={"Home"} />
                            </ListItem>
                            <ListItem button onClick={() => this.setState({ drawercontent: 'Current Client' })} id="disabled"  disabled={local} key={3}>
                                <ListItemIcon style={{color:'#1d89e4'}}><AddcircleOutlineIcon /></ListItemIcon>
                                <ListItemText primary={"Current Client"} />
                            </ListItem>
                            <ListItem button onClick={() => this.setState({ drawercontent: 'User Issues' })} id="disabled"  disabled={local} key={4} >
                                <ListItemIcon style={{color:'#1d89e4'}}><ErrorOutlineIcon /></ListItemIcon>
                                <ListItemText primary={"User Issues"} />
                            </ListItem>
                            <ListItem button onClick={() => this.setState({ drawercontent: 'Help' })} key={5} id="disabled" disabled={local}>
                                <ListItemIcon style={{color:'#1d89e4'}}><HelpOutlineIcon /></ListItemIcon>
                                <ListItemText primary={"Help"} />
                            </ListItem>
                            {/* <ListItem button onClick={() => this.setState({ drawercontent: 'SMaster' })} key={6}>
                                <ListItemIcon  style={{color:'#1d89e4'}} ><SupervisorAccountIcon /></ListItemIcon>
                                <ListItemText primary={"SMaster"} />
                            </ListItem> */}
                        </List>
                        <Divider />
                    </Drawer>
                </div>
                {/* </Grid> */}
            </div>
           
        )
    }
}

export default Dashboard
