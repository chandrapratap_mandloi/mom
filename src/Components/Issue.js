import React, { Component } from 'react'
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import BaseURL from '../BaseURL';
import axios from 'axios'
import SpinnerPage from './SpinnerPage'
import Button from '@material-ui/core/Button';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import TextField from '@material-ui/core/TextField';
import HelpOutlineIcon from '@material-ui/icons/Help'
import Grid from '@material-ui/core/Grid';
import CryptoJS from 'crypto-js'
import Decrypt from './Decrypt'
class Issue extends Component {
    constructor(props) {
        super(props)
        var taskid=CryptoJS.AES.decrypt(window.localStorage.getItem('task'), 'secret key 123');
        var task_id=JSON.parse(taskid.toString(CryptoJS.enc.Utf8));
        // var task_id = window.localStorage.getItem('task');
        // var user = window.localStorage.getItem('user');
        this.state = {
            issues: '',
            errors: {},
            taskid: task_id,
            loading: false,
            userData: Decrypt().user
        }
    }
    handleChange = (event) => {
        this.setState({
            issues: event.target.value
        })
    }
    handleValidation = () => {
        let errors = {};
        let formIsValid = true;
        var addissues = document.getElementById('outlined-name').value;
        if (addissues.length === 0) {
            formIsValid = false;
            toast.error('This Field is required!!', { autoClose: 7000 })
            errors["task"] = "This Field is required!!";
        }
        else {
            formIsValid = true;
            errors["task"] = "";
        }
        this.setState({ errors: errors });
        return formIsValid;
    }
    handleSubmit = (event) => {
        this.setState({ loading: true })
        const { userData, taskid } = this.state
        event.preventDefault()
        const data = {
            issues: this.state.issues,
        }
        if (this.handleValidation()) {
            const headers = {
                'Authorization': userData.data.token
            }
            axios.put(`${BaseURL}/tasks/issue/${taskid}`, data, {
                headers: headers
            })
                .then(response => {
                    if (response.data.success) {
                        toast.success("Issue Added", { autoClose: 7000 })
                        this.setState({ loading: false })
                    }
                })
                .catch(error => {
                    console.log(error, 'error')
                    this.setState({ loading: false })
                })
            // toast.error('Ifff', { autoClose: 7000 })

        }
        else {
            this.setState({ loading: false })
        }
    }
    render() {
        return (
            // display: flex;
            // justify-content: center;
            // padding-top: 6%;

            <div style={{ display: 'flex', justifyContent: 'center', paddingTop: '6%' }}>
                <Paper style={{ width: '80%', justifyContent: 'center', textAlign: 'right' }}>
                    <div>
                        <ToastContainer autoClose={7000} />
                        <SpinnerPage localData={this.state} />
                    </div>
                    <form onSubmit={this.handleSubmit}>
                        {/* <Paper> */}
                            <Grid container style={{ display: 'flex', justifyContent: 'center' }} >
                                <Grid item xs={12} style={{ display: 'flex', justifyContent: 'center' }}>
                                    <Typography variant="h5" component="h3">
                                        Add Issue
                            </Typography>
                                </Grid>
                                <Grid item xs={12} style={{ display: 'flex', justifyContent: 'center' }}>
                                    <Typography variant="h5" component="h3">
                                        <TextField
                                            // className="texvalid"
                                            id="outlined-name"
                                            type="text"
                                            label={<HelpOutlineIcon />}
                                            name="name"
                                            margin="normal"
                                            variant="outlined"
                                            onChange={this.handleChange}
                                            value={this.state.task}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            placeholder="Add Issues"
                                            autoComplete='text'
                                        />
                                    </Typography>
                                </Grid>
                                <Grid item style={{ textAlign: 'center', alignSelf: 'center' }}>
                                    <Typography>
                                        <Button type="submit" style={{backgroundColor:'#1d89e4',color:'white'}} variant="contained">
                                            Submit
            </Button>
                                    </Typography>
                                </Grid>
                            </Grid>
                        {/* </Paper> */}
                    </form>
                </Paper>
            </div>
        )
    }
}

export default Issue
