import React, { Component } from 'react'
import Modal from 'react-modal';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import 'react-toastify/dist/ReactToastify.css';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import Email from '@material-ui/icons/Email'
import AddIcon from '@material-ui/icons/Add'
import RemoveIcon from '@material-ui/icons/Remove'
// import BaseURL from '../BaseURL';
import axios from 'axios'
import Decrypt from './Decrypt'
// import CryptoJS from 'crypto-js'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';

const customStyles = {
  content: {
    // backgroundColor: '#ff7f50',
    backgroundColor: '#1d89e4',
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    maxWidth: 'calc(100vw - 2rem)',
    maxHeight: 'calc(80vh)'
    // max-width: calc(100vw - 2rem);
    // max-height: calc(100vh - 2rem);
  }
};
class SendReport extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      modalIsOpen: this.props.isVisible,
      errors: {},
      // taskid: task_id,
      adminComment:this.props.adminComment,
      userData: Decrypt().user,
      reportto:[1],
      reportcc:[1],
      reportbcc:[1],
      email:[],
      emailcc:[],
      emailbcc:[]
    }
    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }
  openModal() {
    this.setState({ modalIsOpen: true });
  }

  afterOpenModal() {
    this.subtitle.style.color = '#f00';
  }

  closeModal() {
    this.props.cbClose(false)
    // this.setState({ modalIsOpen: false, toggle: false });
  }
  
  handleClientChange = (event) => {
    this.setState({
      client: event.target.value
    })
  }
  handleissueChange = (event) => {
    this.setState({
      issues: event.target.value
    })
  }
  handlePriorityChange = (event) => {
    this.setState({
      help: event.target.value
    })
  }
  componentDidMount() {
    Modal.setAppElement('body');
  }

    handleChange = (event) => {
        this.setState({
            adminComment: event.target.value
        })
    }
    handleEmailChange = (index,event) => {
        const dataArr = this.state.email
        dataArr[index] = event.target.value
        // dataArr.push(event.target.value)
        this.setState({
            email: dataArr
        },console.log(this.state.email))
    }
    handleEmailCCChange = (index,event) => {
        const dataArrcc = this.state.emailcc
        dataArrcc[index] = event.target.value
        // dataArr.push(event.target.value)
        this.setState({
            emailcc: dataArrcc
        },console.log(this.state.emailcc))
    }
    handleEmailBCCChange = (index,event) => {
        const dataArrbcc = this.state.emailbcc
        dataArrbcc[index] = event.target.value
        // dataArr.push(event.target.value)
        this.setState({
            emailbcc: dataArrbcc
        },console.log(this.state.emailbcc))
    }
    handleaddField (index) {
        const dataArr = this.state.reportto
        dataArr.push(index+1)
        this.setState({
            reportto:dataArr
        })
    }
    handleremoveField(index) {
        const dataArr = this.state.reportto
        dataArr.pop(index)
        this.setState({
            reportto:dataArr
        })
    }
    handleccaddField (index) {
        const dataArr = this.state.reportcc
        dataArr.push(index+1)
        this.setState({
            reportcc:dataArr
        })
    }
    handleccremoveField(index) {
        const dataArr = this.state.reportcc
        dataArr.pop(index)
        this.setState({
            reportcc:dataArr
        })
    }
    handlebccaddField (index) {
        const dataArr = this.state.reportbcc
        dataArr.push(index+1)
        this.setState({
            reportbcc:dataArr
        })
    }
    handlebccremoveField(index) {
        const dataArr = this.state.reportbcc
        dataArr.pop(index)
        this.setState({
            reportbcc:dataArr
        })
    }
    transformArray = data => {
        let str = '';
        data.map((result, index) => {
        str = `${result},${str}`;
        })
        return str.slice(0, -1);
    }
    
    handleSubmit = (event) => {
        const {userData,email,emailcc,emailbcc} = this.state
        const headers = {
            'Authorization': userData.data.token   
        }
        const data = {
                    userTask:this.props.reportdata,
                    to:this.transformArray(email),
                    cc:this.transformArray(emailcc),
                    bcc:this.transformArray(emailbcc)
        }
  
        axios.post(`////localhost:3000/SendReport`, data, {
        // axios.post(`//localhost:5000/api/tasks/testNodeMailer`, data, {
        // axios.post(`${BaseURL}/tasks/sendreport`, data, {
            headers: headers
        })
        .then(response => {
            console.log('****',response.data)
            if(response.data.success){
                this.props.cbClose(false, response.data.success)
            }
        })
        .catch(error => {
            console.log(error.data, 'error')
            this.setState({ loading: false })
        })
    }
  render() {
      const {reportto, reportcc, reportbcc} =this.state
    return (
      <Modal
        isOpen={this.props.isVisible}
        style={customStyles}
        contentLabel="Example Modal"
      >
        <CloseIcon onClick={this.closeModal} />
        <Paper >
        <ValidatorForm
            ref="form"
            onSubmit={this.handleSubmit}
            onError={errors => console.log('error', errors)}
        >
            <Grid container style={{ display: 'flex', justifyContent: 'center', paddingTop:'6%', color:'white' }}  spacing={4} >
            <Grid container spacing={4} >
            <Grid item xs={1}></Grid>
              <Grid item xs={10} style={{ display: 'flex', justifyContent: 'center', backgroundColor:'#1d89e4' }}>
                <Typography variant="h5" component="h3">
                  Send Report
                  </Typography>
              </Grid>
              <Grid item xs={1}></Grid>
              </Grid>
              <Grid item xs={10} style={{ display: 'flex', justifyContent: 'center' }}>
                <Typography style={{color:'black'}} variant="h5" component="h3">
                    To:
                {reportto.map((addfield, index) =>
                <Grid container spacing={4} > 
                    <Grid item xs={8}>
                     <TextValidator
                     style={{height:80}}
                     label="Email"
                     type="email"
                     name="email"
                     margin="normal"
                     variant="outlined"
                     onChange={()=>this.handleEmailChange(index,event)}
                     id="email"
                     autoComplete="email"
                     value={this.state.email[index]}
                     InputLabelProps={{
                         shrink: true,
                     }}
                     InputProps={{
                         endAdornment: (
                           <InputAdornment position="end">
                             <IconButton
                               edge="end"
                             >
                                 <Email/>
                             </IconButton>
                           </InputAdornment>
                         ),
                       }}
                     // placeholder="Your Email"
                     validators={['required', 'isEmail']}
                     errorMessages={['this field is required', 'email is not valid']}
                 />
                  
                 </Grid>
                 <Grid item xs={2} style={{alignSelf:'center'}}>
                 <Button type="button" variant="contained" onClick={()=>this.handleaddField(index)} style={{backgroundColor:'#1d89e4',color:'white'}} >
                 <AddIcon style={{color:'white'}} />  
                </Button>
                </Grid>
                <Grid item xs={2} style={{alignSelf:'center'}}>
                 <Button type="button" variant="contained" onClick={()=>this.handleremoveField(index)} style={{backgroundColor:'#1d89e4',color:'white'}} disabled={this.state.reportto.length===1} >
                 <RemoveIcon style={{color:'white'}} />  
                </Button>
                </Grid>
                 </Grid>
                )}
                </Typography>
              </Grid>


              <Grid item xs={10} style={{ display: 'flex', justifyContent: 'center' }}>
                <Typography style={{color:'black'}} variant="h5" component="h3">
                    CC:
                {reportcc.map((addfield, index) =>
                <Grid container spacing={4} > 
                    <Grid item xs={8}>
                     <TextValidator
                     style={{height:80}}
                     label="Email"
                     type="email"
                     name="email"
                     margin="normal"
                     variant="outlined"
                     onChange={()=>this.handleEmailCCChange(index,event)}
                     id="email"
                     autoComplete="email"
                     value={this.state.emailcc[index]}
                     InputLabelProps={{
                         shrink: true,
                     }}
                     InputProps={{
                         endAdornment: (
                           <InputAdornment position="end">
                             <IconButton
                               edge="end"
                             >
                                 <Email/>
                             </IconButton>
                           </InputAdornment>
                         ),
                       }}
                     // placeholder="Your Email"
                     validators={['isEmail']}
                     errorMessages={['email is not valid']}
                 /> 
                  
                 </Grid>
                 <Grid item xs={2} style={{alignSelf:'center'}}>
                 <Button type="button" variant="contained" onClick={()=>this.handleccaddField(index)} style={{backgroundColor:'#1d89e4',color:'white'}} >
                 <AddIcon style={{color:'white'}} />  
                </Button>
                </Grid>
                <Grid item xs={2} style={{alignSelf:'center'}}>
                 <Button type="button" variant="contained" onClick={()=>this.handleccremoveField(index)} style={{backgroundColor:'#1d89e4',color:'white'}} disabled={this.state.reportcc.length===1} >
                 <RemoveIcon style={{color:'white'}} />  
                </Button>
                </Grid>
                 </Grid>
                )}
                </Typography>
              </Grid>


              <Grid item xs={10} style={{ display: 'flex', justifyContent: 'center' }}>
                <Typography style={{color:'black'}} variant="h5" component="h3">
                    BCC:
                {reportbcc.map((addfield, index) =>
                <Grid container spacing={4} > 
                    <Grid item xs={8}>
                     <TextValidator
                     style={{height:80}}
                     label="Email"
                     type="email"
                     name="email"
                     margin="normal"
                     variant="outlined"
                     onChange={()=>this.handleEmailBCCChange(index,event)}
                     id="email"
                     autoComplete="email"
                     value={this.state.emailbcc[index]}
                     InputLabelProps={{
                         shrink: true,
                     }}
                     InputProps={{
                         endAdornment: (
                           <InputAdornment position="end">
                             <IconButton
                               edge="end"
                             >
                                 <Email/>
                             </IconButton>
                           </InputAdornment>
                         ),
                       }}
                     // placeholder="Your Email"
                     validators={[ 'isEmail']}
                     errorMessages={[ 'email is not valid']}
                 /> 
                  
                 </Grid>
                 <Grid item xs={2} style={{alignSelf:'center'}}>
                 <Button type="button" variant="contained" onClick={()=>this.handlebccaddField(index)} style={{backgroundColor:'#1d89e4',color:'white'}} >
                 <AddIcon style={{color:'white'}} />  
                </Button>
                </Grid>
                <Grid item xs={2} style={{alignSelf:'center'}}>
                 <Button type="button" variant="contained" onClick={()=>this.handlebccremoveField(index)} style={{backgroundColor:'#1d89e4',color:'white'}} disabled={this.state.reportbcc.length===1} >
                 <RemoveIcon style={{color:'white'}} />  
                </Button>
                </Grid>
                 </Grid>
                )}
                </Typography>
              </Grid>


              <Grid  container spacing={4} style={{ textAlign: 'center', alignSelf: 'center',  paddingBottom:'2%' }}>
              <Grid item xs={12}>
                <Typography>
                  <Button type="submit" variant="contained" style={{backgroundColor:'#1d89e4',color:'white'}} >
                    Send Report
                </Button>
                </Typography>
                </Grid>
              </Grid>
            </Grid>
            </ValidatorForm>
          {/* </form> */}
        </Paper>
      </Modal>

    )
  }
}

export default SendReport
