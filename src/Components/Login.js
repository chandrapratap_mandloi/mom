import React, { Component } from 'react';
import '../CSS/register.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
// import { faLock } from '@fortawesome/free-solid-svg-icons';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
// import { faMobileAlt } from '@fortawesome/free-solid-svg-icons';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Email from '@material-ui/icons/Email'
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import BaseURL from '../BaseURL';
import axios from 'axios';
import Modal from 'react-modal';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import SpinnerPage from './SpinnerPage';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import CryptoJS from 'crypto-js'
const customStyles = {
    content: {
        top: '50%',
        left: '40%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};
class Login extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: '',
            password: '',
            toggle: false,
            modalIsOpen: false,
            otp: '',
            loading: false,
            hidden: true
        }
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }
    openModal() {
        this.setState({ modalIsOpen: true });
    }
    componentDidMount() {
        // custom rule will have name 'isPasswordMatch'
        var isAuthenticated = window.localStorage.getItem('isAuthenticated')
        if(isAuthenticated===null)
        {
            this.props.history.push('/login')
        }
        else if(isAuthenticated){
            this.props.history.push('/dashboard')
        }
    }
    afterOpenModal() {
        this.subtitle.style.color = '#f00';
    }

    closeModal() {
        this.setState({ modalIsOpen: false, toggle: false });
    }
    updatePassword = () => {
        const obj = {
            email: this.state.email
        }
        axios.post(`${BaseURL}/auth/forgot-password`, obj)
            .then(response => {
                if (response.data.success === true) {
                    // alert('OTP sent to your email')
                    toast.success("OTP sent to your email", { autoClose: 7000 })
                    this.setState({ toggle: true, loading: false })
                }
                else {
                    this.setState({ loading: false })
                }

            })
            .catch(error => { console.log(error, 'error') })
    }
    resetPassword = () => {
        const obj = {
            email: this.state.email,
            password: this.state.password,
            otp: this.state.otp
        }
        axios.post(`${BaseURL}/auth/reset-password`, obj)
            .then(response => {
                if (response.data.success === true) {
                    toast.success("Password Updated", { autoClose: 7000 })
                    this.setState({ modalIsOpen: false, loading: false })
                }
                else {
                    this.setState({ loading: false })
                }

            })
            .catch(error => { 
                console.log(error, 'error') },
                this.setState({ loading: false })
            )
    }
    handleEmailChange = (event) => {
        console.log(event.target.value)
        this.setState({
            email: event.target.value
        })
    }
    handlePasswordChange = (event) => {
        this.setState({
            password: event.target.value
        })
    }
    handleOTPChange = (event) => {
        this.setState({
            otp: event.target.value
        })
    }
    handleSubmit = (event) => {
        this.setState({ loading: true })
        event.preventDefault()
        axios.post(`${BaseURL}/auth/login`, this.state)
            .then(response => {
                if (response.data.success === true) {
                    this.setState({ loading: false })
                    // window.localStorage.setItem('user', JSON.stringify(response.data))
                    // window.localStorage.setItem('isAuthenticated', true)
                    window.localStorage.setItem('user',CryptoJS.AES.encrypt(JSON.stringify(response.data),'secret key 123'))
                    // window.localStorage.setItem('isAuthenticated',CryptoJS.AES.encrypt(JSON.stringify(true), 'secret key 123'))
                    window.localStorage.setItem('isAuthenticated',true)                    
                    setTimeout(() => toast.success("Login Successfully", { autoClose: 3000 })
                    ,10)
                    if (response.data.data.user.todayBool === true) {
                        window.localStorage.setItem('flag', true)
                        this.setState({ loading: false })
                    }
                    else { 
                        window.localStorage.setItem('flag', false)
                        this.setState({ loading: false })
                    }
                    this.props.history.push('/dashboard')
                    // setTimeout(() => toast.success("Login Successfully", { autoClose: 3000 })
                    // ,10)
                }
                else {
                    this.setState({ loading: false })
                }
            })
            .catch(error => {
                console.log(error, 'error')
                this.setState({ loading: false })
                toast.error('Invalid login credentials.', { autoClose: 7000 })
            })
    }
    handleUpdatePassword() {
        return (
            <div>
                <div>
                    <SpinnerPage localData={this.state} />
                </div>
                <ValidatorForm
                    ref="form"
                    onSubmit={this.updatePassword}
                    onError={errors => console.log('error', errors)}
                >
                    <div className="form-group">
                        <TextValidator
                            label="Email"
                            type="email"
                            name="email"
                            margin="normal"
                            variant="outlined"
                            onChange={this.handleEmailChange}
                            id="email"
                            autoComplete="email"
                            value={this.state.email}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            InputProps={{
                                endAdornment: (
                                  <InputAdornment position="end">
                                    <IconButton
                                      edge="end"
                                    >
                                        <Email/>
                                    </IconButton>
                                  </InputAdornment>
                                ),
                              }}
                            // placeholder="Your Email"
                            validators={['required', 'isEmail']}
                            errorMessages={['this field is required', 'email is not valid']}

                        />
                    </div>
                    <div className="form-group form-button">
                        <input type="submit" className="form-submit" value="Log in" />
                    </div>
                </ValidatorForm>

            </div>
        )
    }
    handleResetPassword() {
        return (
            <div>
                <div>
                    <SpinnerPage localData={this.state} />
                </div>
                <ValidatorForm
                    ref="form"
                    onSubmit={this.resetPassword}
                    onError={errors => console.log('error', errors)}
                    style={{zindex:-1}}
                >
                    <div className="form-group">
                        <TextValidator
                            // label={<FontAwesomeIcon icon={faMobileAlt} className="image" />}
                            label="Enter OTP"
                            type="text"
                            name="your_otp"
                            id="your_otp"
                            autoComplete="false"
                            onChange={this.handleOTPChange}
                            placeholder="Enter OTP"
                            value={this.state.otp}
                            validators={['required']}
                            errorMessages={['this field is required']}
                            // autoComplete='false'
                            margin="normal"
                            variant="outlined"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            defaultValue={this.state.otp}
                        />
                    </div>
                    <div className="form-group">
                        <TextValidator
                            type="password"
                            label="Password"
                            name="your_pass"
                            onChange={this.handlePasswordChange}
                            id="your_pass"
                            margin="normal"
                            variant="outlined"
                            value={this.state.password}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            placeholder="Password"
                            validators={['required']}
                            errorMessages={['this field is required']}
                        />
                    </div>
                    <div className="form-group form-button">
                        <input type="submit" name="signin" id="signin" className="form-submit" value="Log in" />
                    </div>
                </ValidatorForm>
            </div>
        )
    }
    handleMouseDownPassword=()=>{
        this.setState({hidden:false})
    }
    handleClickShowPassword=()=> {
        this.setState({ hidden: !this.state.hidden })
      }
    render() {
        const { toggle } = this.state
        return (
            <div className="main" style={{marginTop:'12%'}}>
                <div>
                    <ToastContainer autoClose={7000} />
                    <SpinnerPage localData={this.state} />
                </div>
                <section className="sign-in">
                    <div className="container">
                        <div className="signin-content">
                            <div className="signin-image">
                                <img src={require('../images/signin-image.jpg')} alt="sign up" />
                                <a href="/register">Create an account</a>
                            </div>
                            <div className="signin-form">
                                <h2 className="form-title">Sign up</h2>

                                <ValidatorForm
                                    ref="form"
                                    onSubmit={this.handleSubmit}
                                    onError={errors => console.log('error', errors)}
                                >
                                    <div className="form-group">
                                        <TextValidator
                                           type="email"
                                            name="email"
                                            label="Email"
                                            margin="normal"
                                            variant="outlined"
                                            onChange={this.handleEmailChange}
                                            id="email"
                                            value={this.state.email}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            InputProps={{
                                                endAdornment: (
                                                  <InputAdornment position="end">
                                                    <IconButton
                                                      edge="end"
                                                    >
                                                        <Email/>
                                                    </IconButton>
                                                  </InputAdornment>
                                                ),
                                              }}
                                            // placeholder="Your Email"
                                            validators={['required', 'isEmail']}
                                            errorMessages={['this field is required', 'email is not valid']}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <TextValidator
                                            type={this.state.hidden ? "password" : "text"}
                                            name="your_pass"
                                            label="Password"
                                            margin="normal"
                                            variant="outlined"
                                            onChange={this.handlePasswordChange}
                                            id="your_pass"
                                            value={this.state.password}
                                            autoComplete="false"
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            // placeholder="Password"
                                            validators={['required']}
                                            errorMessages={['this field is required']}
                                            InputProps={{
                                                endAdornment: (
                                                  <InputAdornment position="end">
                                                    <IconButton
                                                      edge="end"
                                                      aria-label="toggle password visibility"
                                                      onClick={this.handleClickShowPassword}
                                                      onMouseDown={this.handleMouseDownPassword}
                                                    >
                                                      {this.state.hidden ? <VisibilityOff /> : <Visibility />}
                                                    </IconButton>
                                                  </InputAdornment>
                                                ),
                                              }}
                                        />

                                    </div>
                                    <div className="form-group form-button" style={{ display: 'flex' }}>
                                        <input type="submit" name="signin" id="signin" className="form-submit" value="Log in" />
                                        <button type="button" style={{ marginTop: '7%' }} className="Button1" onClick={this.openModal}>Forgot Password</button>
                                    </div>
                                </ValidatorForm>
                            </div>
                        </div>
                    </div>
                </section>
                <div>
                    <Modal
                        isOpen={this.state.modalIsOpen}
                        // onAfterOpen={this.afterOpenModal}
                        // onRequestClose={this.closeModal}
                        style={customStyles}
                        contentLabel="Example Modal"
                    >
                        <FontAwesomeIcon icon={faTimes} onClick={this.closeModal} className="image" />
                        <h2 className="modalStyle" ref={subtitle => this.subtitle = subtitle}>Reset Password</h2>
                        {toggle === false ? this.handleUpdatePassword() : this.handleResetPassword()}
                    </Modal>
                </div>
            </div>
        )
    }
}

export default Login
