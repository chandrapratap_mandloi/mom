import React, { Component } from 'react'
import Modal from 'react-modal';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import BaseURL from '../BaseURL';
import axios from 'axios';
import CryptoJS from 'crypto-js'
import Decrypt from './Decrypt';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
// import Priority from './Priority'
const priority = ['Low', 'Medium', 'High'];
const textstyle = {width:150, height:40}
const customStyles = {
  content: {
    // backgroundColor: '#ff7f50',
    backgroundColor: '#1d89e4',
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
};
class EditTask extends Component {
  constructor(props) {
    super(props)
    // var task_id = window.localStorage.getItem('task');
    // var user = window.localStorage.getItem('user');
    var taskid=CryptoJS.AES.decrypt(window.localStorage.getItem('task'), 'secret key 123');
    var task_id=JSON.parse(taskid.toString(CryptoJS.enc.Utf8));
    this.state = {
      data: [],
      modalIsOpen: this.props.isVisible,
      task: this.props.task,
      client: this.props.client,
      errors: {},
      issues: this.props.issues,
      taskid: task_id,
      help: this.props.help,
      // userData: JSON.parse(user),
      userData:Decrypt().user
    }
    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }
  openModal() {
    this.setState({ modalIsOpen: true });
  }

  afterOpenModal() {
    this.subtitle.style.color = '#f00';
  }

  closeModal() {
    this.props.cbClose(false)
    // this.setState({ modalIsOpen: false, toggle: false });
  }
  handleChange = (event) => {
    this.setState({
      task: event.target.value
    })
  }
  handleClientChange = (event) => {
    this.setState({
      client: event.target.value
    })
  }
  handleissueChange = (event) => {
    this.setState({
      issues: event.target.value
    })
  }
  handlePriorityChange = (event) => {
    this.setState({
      help: event.target.value
    })
  }
  componentDidMount() {
    // this.setState({ loading: true })
    Modal.setAppElement('body');
    const { userData } = this.state
    const AuthStr = (userData.data.token);
    axios.get(`${BaseURL}/users/client/${userData.data.user._id}`, { headers: { Authorization: AuthStr } })
      .then(response => {
        this.setState({
          data: response.data.data,
          loading: false
        })
      })
      .catch((error) => {
        console.log('error ', error);
        this.setState({ loading: false })
      });
  }

  handleValidation = () => {
    let errors = {};
    let formIsValid = true;
    var edittask = document.getElementById('outlined-name').value;
    var editissue = document.getElementById('issues').value;
    var editclient = document.getElementById('client').value;
    var editpriority = document.getElementById('priority').value;
    if (edittask.length === 0) {
      formIsValid = false;
      toast.error('Add Task!!', { autoClose: 7000 })
      errors["task"] = "Add Task!!!";
    }
    else if(editissue.length===0){
      formIsValid = false;
      toast.error('Add Issue!!', { autoClose: 7000 })
    }
    else if(editclient.length===0){
      formIsValid = false;
      toast.error('Add Client!!', { autoClose: 7000 })
    }
    else if(editpriority.length===0){
      formIsValid = false;
      toast.error('Add Help!!', { autoClose: 7000 })
    }
    else {
      formIsValid = true;
      errors["task"] = "";
    }
    this.setState({ errors: errors });
    return formIsValid;
  }
  handleSubmit = (event) => {
    const { userData, taskid } = this.state
    event.preventDefault()
    const data = {
      task: this.state.task,
      client: this.state.client,
      issues:this.state.issues,
      help:this.state.help
    }
    if (this.handleValidation()) {
      const headers = {
        'Authorization': userData.data.token
      }
      axios.put(`${BaseURL}/tasks/${taskid}`, data, {
        headers: headers
      })
        .then(response => {
          if (response.data.success) {
            this.props.cbClose(false, response.data.success)
            // this.closeModal()
          }
        })
        .catch(error => {
          console.log(error.data, 'error')
        })
    }
  }
  render() {
    const { data } = this.state
    return (
      <Modal
        isOpen={this.props.isVisible}
        // onAfterOpen={this.afterOpenModal}
        // onRequestClose={this.closeModal}
        style={customStyles}
        contentLabel="Example Modal"
      >
        <CloseIcon onClick={this.closeModal} />
        <Paper >
          <form
            onSubmit={this.handleSubmit}
          >
            <Grid container style={{ display: 'flex', justifyContent: 'center' }}  spacing={4} >
              <Grid item xs={12} style={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="h5" component="h3">
                  Edit Task
                  </Typography>
              </Grid>
              <Grid item xs={12} style={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="h5" component="h3">
                <TextareaAutosize 
                  // aria-label="empty textarea" 
                  // placeholder="Empty" 
                  aria-label="minimum height" 
                  rows={3}
                  style={textstyle}
                  id="outlined-name"
                  type="text"
                  label="Task"
                  margin="normal"
                  name="name"
                  variant="outlined"
                  onChange={this.handleChange}
                  value={this.state.task}
                  // placeholder="Add Task"
                  autoComplete='text'
                  // InputLabelProps={{
                  //   shrink: true,
                  // }}
                  placeholder="Edit Task"
                />
                </Typography>
              </Grid>
              <Grid>
              <TextField
                id="client"
                style={textstyle}
                select
                label="Select Client"
                value={this.state.client}
                onChange={this.handleClientChange}
                SelectProps={{
                  native: true,
                }}
                InputLabelProps={{
                  shrink: true,
                }}
                margin="normal"
                variant="outlined"
              >
                <option value="none">
                  None
                  </option>
                {data.map((data, index) =>
                  <option key={index} value={data}>
                    {data}
                  </option>
                )}
              </TextField>
              </Grid>
              <Grid item xs={12} style={{ display: 'flex', justifyContent: 'center' }}>
                <Typography variant="h5" component="h3">
                  <TextField
                    id="issues"
                    style={textstyle}
                    type="text"
                    label="Issue"
                    name="name"
                    margin="normal"
                    variant="outlined"
                    onChange={this.handleissueChange}
                    value={this.state.issues}
                    InputLabelProps={{
                      shrink: true,
                    }}
                    placeholder="Edit Issue"
                    autoComplete='text'
                  />
                </Typography>
              </Grid>
              {/* priority */}
              <Grid item xs={12} style={{ display: 'flex', justifyContent: 'center' }}>
                <TextField
                  id="priority"
                  style={textstyle}
                  select
                  label="Select Priority"
                  value={this.state.help}
                  onChange={this.handlePriorityChange}
                  SelectProps={{
                    native: true,
                  }}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  margin="normal"
                  variant="outlined"
                >
                  <option value="">
                    None
                    </option>
                  {priority.map((data, index) =>
                    <option key={index} value={data}>
                      {data}
                    </option>
                  )}
                </TextField>
              </Grid>
              <Grid item style={{ textAlign: 'center', alignSelf: 'center' }}>
                <Typography>
                  <Button type="submit" variant="contained" color="primary">
                    Submit
            </Button>
                </Typography>
              </Grid>
            </Grid>
          </form>
          <div>
            <ToastContainer autoClose={7000} />
          </div>
        </Paper>
      </Modal>

    )
  }
}

export default EditTask
