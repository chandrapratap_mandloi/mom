import React, { Component } from 'react';
import BaseURL from '../BaseURL';
import axios from 'axios';
import Paper from '@material-ui/core/Paper';
import EditIcon from '@material-ui/icons/Edit'
import EditComment from './EditComment'
import SpinnerPage from './SpinnerPage'
import Decrypt from './Decrypt'
class SMaster extends Component {
    constructor(props) {
        super(props)
        // var user = window.localStorage.getItem('user');
        this.state = {
            userData: Decrypt().user,
            data: [],
            modal: false,
            loading: true,
            selectedIndex:undefined
        }
    }
    fetchData() {
        const { userData } = this.state
        const AuthStr = (userData.data.token);
        axios.get(`${BaseURL}/tasks/todaytask`, { headers: { Authorization: AuthStr } })
        .then(response => {
            if(response.data.success===true){
               this.setState({
                    loading: false,
                   data: response.data.data
               })
            }
            else{
                this.setState({
                    loading: false
                })
            }
        })
        .catch((error) => {
            this.setState({ loading: false})
            console.log('error ', error);
        });
    }
    modalclose = (value, isUpdated) => {
        this.setState({
            modal: value
        })
        if (isUpdated) {
            this.fetchData()
        }
    }
    componentDidMount(){
        this.fetchData()
    }
    addComment (index) {
        this.setState({modal: true,selectedIndex:index})
    }
    showUsersTask() {
        const {data} = this.state
        return (
            <table >
            <tbody>
                <tr style={{ backgroundColor: '#1d89e4', color:'white' }}>
                    <th>Name</th>
                    <th>Task</th>
                    <th>Client</th>
                    <th>Issue</th>
                    <th>Priority</th>
                    <th>Comment</th>
                </tr>
                {data.map((tasks,index)=> {
                    const bg = tasks.help === 'High' ? {backgroundColor: 'blue',flex:2} : tasks.help === 'Medium' ? { backgroundColor: 'yellow',flex:2} : { backgroundColor: 'green',flex:2}
                    return(
                        <tr style={{ backgroundColor: 'coral' , color:'white'}}>
                    <td>
                        {tasks.userName}  
                    </td>
                    <td>
                        {tasks.task}
                    </td>
                    <td>
                      {tasks.client}
                    </td>
                    <td >
                        {tasks.issues}
                    </td>
                    <td key={index}>
                        <div style={{display: "flex",flex: 1,flexdirection: "row"}}>
                            <div style={bg}></div>
                            <div style={{flex: 20}}>{tasks.help}</div>
                        </div>
                    </td>
                    <td>
                        {tasks.adminComment}
                        {this.state.selectedIndex===index && <EditComment userId={tasks._id} adminComment={tasks.comment}  cbClose={this.modalclose}  isVisible={this.state.modal} />}
                        {/* onClick={()=>this.addComment(tasks,index)} */}
                        {/* onClick={() => this.setState({ modal: true })} */}
                        <EditIcon onClick={()=>this.addComment(index)} style={{height: 11 ,paddingLeft : 10}}/>
                    </td>
                </tr>
                    )
                }
                )}
            </tbody>
        </table>
        )
    }
    handleUSerTasks(){
        return(
            <div>
                No User has added task
            </div>
        )
    }
    render() {
        const {data} = this.state
        return (
            <div style={{display:'flex', justifyContent:'center', marginTop:'10%'}}>
                <div>
                    <SpinnerPage localData={this.state} />
                </div>
                <Paper style={{width:'80%'}}>
                    { data.length === 0 ? this.handleUSerTasks() : this.showUsersTask() }
                </Paper>
            </div>
        )
    }
}

export default SMaster
