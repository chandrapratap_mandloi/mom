import React, { Component } from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import '../CSS/priority.css';
import Paper from '@material-ui/core/Paper';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import Select from '@material-ui/core/Select';
// import HelpOutlineIcon from '@material-ui/icons/Help'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
// import FormHelperText from '@material-ui/core/FormHelperText';
import BaseURL from '../BaseURL';
import axios from 'axios'
import Decrypt from './Decrypt'
import CryptoJS from 'crypto-js'
class Priority extends Component {
    constructor(props) {
        super(props)
        var taskid=CryptoJS.AES.decrypt(window.localStorage.getItem('task'), 'secret key 123');
        var task_id=JSON.parse(taskid.toString(CryptoJS.enc.Utf8));
        // var task_id = window.localStorage.getItem('task');
        // var user = window.localStorage.getItem('user');
        this.state = {
            menuopen: false,
            priority: '',
            taskid: task_id,
            errors: {},
            userData: Decrypt().user
        }
    }
    handleClick = () => {
        this.setState({
            menuopen: true
        })
    }
    handleClose = () => {
        this.setState({
            menuopen: false
        })
    }
    handleChange = (event) => {
        const { userData, taskid } = this.state
        const headers = {
            'Authorization': userData.data.token
        }
        this.setState({
            priority: event.target.value
        }, () =>
                this.state.priority === '' ?
                    toast.error("Condition True", { autoClose: 7000 }) :
                    axios.put(`${BaseURL}/tasks/help/${taskid}`, { help: this.state.priority }, {
                        headers: headers
                    })
                        .then(response => {
                            if (response.data.success) {
                                toast.success(this.state.priority, { autoClose: 7000 })
                                this.setState({ loading: false })
                            }
                        })
                        .catch(error => {
                            console.log(error, 'error')
                            this.setState({ loading: false })
                        })
        )
    }
    render() {
        return (
            <div className="prioritydiv">
                <Paper style={{ backgroundColor: '#ff7f50', width: '80%', display: 'flex', justifyContent: 'center' }}>
                    <div>
                        <ToastContainer autoClose={7000} />
                        {/* <SpinnerPage localData={this.state} /> */}
                    </div>
                    <FormControl variant="outlined">
                        <InputLabel htmlFor="outlined-age-simple">
                            Select Priority
                        </InputLabel>
                        <Select
                            style={{ width: 152 }}
                            value={this.state.priority}
                            onChange={this.handleChange}
                            input={<Input id="outlined-age-simple" />}
                        >
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            <MenuItem value={'low'}>
                                <ListItemIcon>
                                    <img src={require('../images/low.png')} style={{ height: 23 }} alt="Low Priority" />
                                </ListItemIcon>
                                Low</MenuItem>
                            <MenuItem value={'medium'}>
                                <ListItemIcon>
                                    <img src={require('../images/medium.png')} style={{ height: 23 }} alt="Medium Priority" />
                                </ListItemIcon>
                                Medium</MenuItem>
                            <MenuItem value={'high'}>
                                <ListItemIcon>
                                    <img src={require('../images/high.png')} style={{ height: 23 }} alt="High Priority" />
                                </ListItemIcon>
                                High
                            </MenuItem>
                        </Select>
                        {/* <FormHelperText>Select Priority</FormHelperText> */}
                    </FormControl>
                </Paper>
            </div>
        )
    }
}

export default Priority
