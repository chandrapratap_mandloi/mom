import React, { Component } from 'react'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import '../CSS/dashboard.css'
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import HighlightOff from '@material-ui/icons/HighlightOff'
export class Trying extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            setOpen:false
        }
    }
    handleDrawerOpen=()=>{
        this.setState({setOpen: true});
    }
    handleDrawerClose=()=>{
        
        this.setState({setOpen:false})
    }
    logout=()=>{
        
        // localStorage.removeItem('user');
        // this.props.history.push('/login');
    }
    render() {
        return (
            <div>
                <div className="root">
                    <AppBar position="static">
                        <Toolbar>
                            <div className="MuiToolbar-root">
                                <IconButton 
                                    className="menuButton" 
                                    edge="start" 
                                    color="inherit"  
                                    aria-label="menu"
                                    onClick={this.handleDrawerOpen}
                                >
                                    <MenuIcon />
                                </IconButton>
                                <Typography className="title" variant="h6" >
                                    News
                                </Typography>
                            </div>
                            <div>
                                <Button color="inherit" onClick={this.logout}>Logout <HighlightOff/></Button>
                            </div>
                        </Toolbar>
                    </AppBar>
                </div>
                <div>
                <Drawer
                    variant="persistent"
                    anchor="left"
                    open={this.state.setOpen}
                >
                    <div>
                        <IconButton onClick={this.handleDrawerClose}>
                            <ChevronLeftIcon /> 
                        </IconButton>
                    </div>
                    <Divider />
                    <List>
                            <ListItem button key={1}>
                                <ListItemIcon><InboxIcon /></ListItemIcon>
                                <ListItemText primary={"text"} />
                            </ListItem>
                    </List>
                    <Divider />
                </Drawer>
                </div>
            </div>
        )
    }
}

export default Trying
